const util = require('../../utils/util.js');
var mta = require('../../utils/mta_analysis.js');
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    addresss:[],
    showAddButton: true,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    mta.Page.init();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function (options) {
    this.initData(options);
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },
  bindClickAddAddress: function(){
    wx.navigateTo({
      url: '/pages/my/addAddress',
    })
  },
  bindClickChooseWxAddress: function(){
    //直接使用微信地址算了
    let that = this;
    wx.chooseAddress({
      success: function (res) {
        let address = {};
        address.phone = res.telNumber;
        address.contact = res.userName;
        address.province = res.provinceName;
        address.city = res.cityName;
        address.area = res.countyName;
        address.street = res.detailInfo;


        let data = address;
        let openid = app.globalData.openid || wx.getStorageSync('openid');
        data.openid = openid;

        wx.request({
          url: app.host + '/user/address/save',
          data: data,
          method: 'POST',
          header: {
            "Content-Type": "application/x-www-form-urlencoded"
          },
          success: function (res) {
            if (res && res.data && res.data.code > 0) {
              wx.hideLoading();
              wx.showToast({
                title: '地址添加成功',
              });
            } else {
              wx.showToast({
                title: '地址添加失败,请售后再试',
              })

            }
          },
          fail: function () {
            wx.hideLoading();
          }
        })

      }
    })

  },
  initData: function(options){
    let openid = app.globalData.openid || wx.getStorageSync('openid');
    this.getAddresss(openid);
  },
  getAddresss: function(openid){
    let that = this;
    wx.showLoading({
      title: '数据加载中'
    })
    wx.request({
      url: app.host + '/user/address/list',
      data: { openid: openid },
      method: 'GET',
      header: {
        "Content-Type": "applciation/json"
      },
      success: function (res) {
        if (res && res.data && res.data.code > 0) {
          wx.hideLoading();
          let addresss = res.data.data.addresss;
          that.setData({ addresss: addresss });

          if(addresss && addresss.length >=1){
            that.setData({ showAddButton: false });
          }

        } else {
          wx.showToast({
            title: '地址列表加载失败,请售后再试',
          })

        }
      },
      fail: function () {
        wx.hideLoading();
      }
    })
  }
})