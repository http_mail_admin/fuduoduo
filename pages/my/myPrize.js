const util = require('../../utils/util.js');
var mta = require('../../utils/mta_analysis.js');
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    mta.Page.init();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function (options) {
    this.initData(options);
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  initData: function (options) {
    let openid = app.globalData.openid || wx.getStorageSync('openid');
    this.getMyActivitys(openid);
  },

  getMyActivitys(openid) {
    let that = this;
    wx.showLoading({
      title: '数据加载中'
    })
    wx.request({
      url: app.host + '/card/myPrize',
      data: { openid: openid },
      method: 'GET',
      header: {
        "Content-Type": "applciation/json"
      },
      success: function (res) {
        if (res && res.data && res.data.code > 0) {
          wx.hideLoading();
          let activitys = res.data.data.activitys;
          that.setData({ activitys: activitys });
        } else {
          wx.showToast({
            title: '活动信息加载失败,请售后再试',
          })

        }
      },
      fail: function () {
        wx.hideLoading();
      }
    })
  }
})