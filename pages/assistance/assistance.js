// pages/assistance/assistance.js
var mta = require('../../utils/mta_analysis.js');
var util = require('../../utils/util.js');
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    cardUser:{},
    cardActivity:{},
    blessList:[],
    cardUserId: 0,
    cardActivityId: 0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (options && options.scene && options.scene !== 'undefined') {
      var scene = decodeURIComponent(options.scene);
      if(scene){
        let sceneParam = util.getQueryString(scene);
        this.data.cardActivityId = sceneParam.id;
        this.data.cardUserId = sceneParam.userId;
      }
    }else{
      this.data.cardActivityId = options.id;
      this.data.cardUserId = options.userId;
    }
    this.initData(options);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function (options) {
    this.initData(options);
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },
  bindClickBless: function(e){
    mta.Event.stat("assistancce", {});

    //保存用户信息
    if (e && e.detail && e.detail.userInfo) {
      app.saveUserInfo(e.detail.userInfo);
    }

    let activityId = this.data.cardActivity.id;
    let userId = this.data.cardUser.id;
    let openid = app.globalData.openid;
    let userInfo = app.globalData.userInfo;

    let data = {};
    data.activityId = activityId;
    data.userId = userId;
    data.openid = app.globalData.openid;
    data.nick = userInfo.nickName;
    data.avator = userInfo.avatarUrl;

    //let data = { activityId: activityId, userId: userId, openid: openid };
    wx.request({
      url: app.host + 'card/bless',
      data: data,
      method: 'POST',
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function (res) {
        let title = res.data.code > 0 ? '赐福成功' : '赐福失败';
        let msg = res.data.msg || '';

        wx.showModal({
          title: title,
          content: msg + '，点击确定看看集福能得到什么？',
          cancelText:'放弃',
          confirmText:'确定',
          success: function (res) {
            if (res.confirm) {
              mta.Event.stat('assistancce', { 'assistance2detail': 'true' });
              wx.navigateTo({
                url: '/pages/detail/detail?id=' + activityId + '&userId=' + userId
              })
            } else if (res.cancel) {
              mta.Event.stat('assistancce', { 'assistance2discard': 'true' });
              console.log('用户点击取消')
            }
          }
        })
      }
    })
  },
  bindClick2Detail: function(e){

    if (e && e.detail && e.detail.userInfo) {
      app.saveUserInfo(e.detail.userInfo);
    }

    console.log(this.data);
    let activityId = this.data.cardActivity.id;
    let userId = this.data.cardUser.id;
    
    wx.navigateTo({
      url: '/pages/detail/detail?id=' + activityId + '&userId=' + userId
    })
  },
  initData: function (options) {
    let that = this;
    this.getActivityDetail(options);
    this.getUserDetail(options);
    this.getBlessList(options);

    let openid = wx.getStorageSync('openid');
    this.setData({openid: openid});

    wx.getUserInfo({
      success: function (res) {
        console.log(res);
        that.setData({ userInfo: res.userInfo });
      }
    });
  },
  getActivityDetail: function (options, callback) {
    var activityId = this.data.cardActivityId;
    var that = this;

    wx.showLoading({
      title: '数据加载中',
    })

    wx.request({
      url: app.host + '/card/detail',
      data: { id: activityId },
      method: 'GET',
      header: {
        "Content-Type": "applciation/json"
      },
      success: function (res) {
        if (res && res.data && res.data.code > 0) {
          wx.hideLoading();
          var cardActivity = res.data.data.activity;
          that.setData({ cardActivity: cardActivity });
        } else {
          wx.showToast({
            title: '活动信息加载失败,请售后再试',
          })

        }
      },
      fail: function () {
        wx.hideLoading();
      }
    })
  },

  getUserDetail: function (options) {

    let that = this;
    let activityId = this.data.cardActivityId;
    var userId = this.data.cardUserId;
    if (userId) {

      wx.request({
        url: app.host + '/card/getUserInfo',
        data: { id: userId },
        method: 'GET',
        header: {
          "Content-Type": "applciation/json"
        },
        success: function (res) {
          that.setData({ cardUser: res.data.data.user });
        }
      })
    }

  },

  getBlessList: function (options) {
    let activityId = this.data.cardActivityId;
    let userId = this.data.cardUserId;
    let that = this;

    wx.request({
      url: app.host + '/card/blessList',
      data: { activityId: activityId, userId: userId },
      method: 'GET',
      header: {
        "Content-Type": "applciation/json"
      },
      success: function (res) {
        if (res && res.data && res.data.code > 0) {
          let blessList = res.data.data.blessList;
          that.setData({ blessList: blessList });
        }
      }
    })
  }
})