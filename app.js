//app.js
var mta = require('/utils/mta_analysis.js');
const ald = require('./utils/ald-stat.js');

App({
  host: 'https://cardapi.sinapp.cn/',
  onLaunch: function (options) {

    mta.App.init({
      "appID": "500587569",
      "eventID": "500587572",
      "lauchOpts": options,
      "statPullDownFresh": true,
      "statShareApp": true,
      "statReachBottom": true
    });

    let that = this;

    let openid = wx.getStorageSync('openid');

    //如果此处从storage中获取到openid，说明之前访问过，则不需login和save
    if(openid){
      that.globalData.openid = openid;
      wx.getUserInfo({
        success: res => {
          // 可以将 res 发送给后台解码出 unionId
          let userInfo = res.userInfo;
          userInfo.openid = that.globalData.openid;
          that.globalData.userInfo = userInfo;

          // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
          // 所以此处加入 callback 以防止这种情况
          if (this.userInfoReadyCallback) {
            this.userInfoReadyCallback(res)
          }
        },
        complete: function () {
          console.log("complete getUserInfo");
        }
      })
    }

    // 登录
    wx.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
        wx.request({
          url: that.host + '/weixin/miniapp/login',
          data: { jsCode: res.code },
          method: 'POST',
          header: {
            "Content-Type": "application/x-www-form-urlencoded"
          },
          success: function (res) {
            if (res && res.data && res.data.code > 0) {
              wx.hideLoading();
              var openid = res.data.data.openid;
              console.log('app.js wx.login send jscode get openid success');
              console.log(openid);
              that.globalData.openid = openid;
              wx.setStorageSync('openid', openid);

              //wx.aldstat.sendOpenid("请传入获取的openid")  // 将openid上报给阿拉丁统计程序
              that.getUserInfo();
            } else {
              wx.showToast({
                title: '活动信息加载失败,请售后再试',
              })

            }
          },
          fail: function () {
            wx.hideLoading();
          }
        })
      }
    })
    
  },

  getUserInfo: function(){
    let that = this;
    // 获取用户信息
    //wx.getSetting({
      //success: res => {
        //if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          wx.getUserInfo({
            success: res => {
              // 可以将 res 发送给后台解码出 unionId
              let userInfo = res.userInfo;
              userInfo.openid = that.globalData.openid;
              that.globalData.userInfo = userInfo;

              wx.request({
                url: that.host + '/weixin/miniapp/save',
                data: userInfo,
                method: 'POST',
                header: {
                  "Content-Type": "application/x-www-form-urlencoded"
                },
                success: function (res) {
                  if (res && res.data && res.data.code > 0) {
                    wx.hideLoading();
                  } else {

                  }
                },
                fail: function () {
                  wx.hideLoading();
                }
              })

              // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
              // 所以此处加入 callback 以防止这种情况
              if (this.userInfoReadyCallback) {
                this.userInfoReadyCallback(res)
              }
            },
            complete: function(){
              console.log("complete getUserInfo");
            }
          })
        //}
      //}
    //})
  },

  globalData: {
    userInfo: null,
    openid:''
  },
  onShow: function(options){
  },
  saveUserInfo: function(userInfo){
    console.log('app.saveUserInfo');
    this.globalData.userInfo = userInfo;
  }
})