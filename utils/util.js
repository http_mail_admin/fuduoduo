const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return [year, month, day].map(formatNumber).join('-') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}

const getQueryString = function(str){

  let theRequest = new Object(); 

  if(str){
    let strArray = str.split('&');
    for (var i = 0; i < strArray.length; i++) {
      theRequest[strArray[i].split("=")[0]] = unescape(strArray[i].split("=")[1]);
    }  
  }

  return theRequest;

}

module.exports = {
  formatTime: formatTime,
  getQueryString: getQueryString
}
